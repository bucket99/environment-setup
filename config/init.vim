set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nohlsearch
set exrc
set guicursor=
set relativenumber
set nu
set nowrap
set hidden
set noerrorbells
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set scrolloff=8
set signcolumn=yes
set shell=zsh
set splitbelow splitright
set bg=dark
set encoding=UTF-8

"markdown config
let g:mkdp_auto_close = 0
let g:mkdp_auto_start = 0

"Plugin declarations
call plug#begin('~/.vim/plugged')
"A plugin for commenting out stuff
Plug 'tpope/vim-commentary'

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
"Plug 'neovim/nvim-lspconfig'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

"
"Plug 'hrsh7th/nvim-cmp'
"Plug 'hrsh7th/nvim-compe' (deprecated)
Plug 'ryanoasis/vim-devicons'
Plug 'vim-airline/vim-airline'

"Git integration. Seems nice but I only really use the blame feature.
Plug 'tpope/vim-fugitive'

"File tree
Plug 'preservim/nerdtree'
"Dev icons that work well with nerd tree
Plug 'kyazdani42/nvim-web-devicons' 

"Project wide grep/find... had hard requirements on ripgrep and fd
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-lua/plenary.nvim'

"Syntax highlighting
Plug 'nvim-treesitter/nvim-treesitter'

" Theme based plugins... oceanic seems better than gruvbox
Plug 'mhartington/oceanic-next'
"Plug 'gruvbox-community/gruvbox'
call plug#end()

"Python lsp
"lua << EOF
"require'lspconfig'.pyright.setup{}
"EOF

" START OceanicNext theme settings
" For Neovim 0.1.3 and 0.1.4
let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" Or if you have Neovim >= 0.1.5
if (has("termguicolors"))
 set termguicolors
endif

" Theme
syntax enable
colorscheme OceanicNext
" END OceanicNexEnd theme settings
"
"colorscheme gruvbox

"START Key Remappings
"
"mode you want to remap, lhs (thing to execute.. in order to execute the remap), rhs
"(thing to do when that remap is hit)

"mapleader is basically the key(s) we want to hit that will act as a 'prefix'
"for our remaps. In other words, they are the key(s) that need to be hit
"before the remaps. This way, we won't conflict with other remaps, or execute
"remaps accidentally
let mapleader = " "
"nnoremap = normal mode, no recursive execution, map (what we're mapping)
nnoremap <leader>ps :lua require('telescope.builtin').grep_string({ search = vim.fn.input("Grep For > ")})<CR>
nnoremap <leader>pf <cmd>lua require('telescope.builtin').find_files()<CR>
nnoremap <leader>f :NERDTreeToggle<CR>
nnoremap <leader>r :NERDTreeFind<CR>

" Open a terminal in a split.
map <leader>tt :new term://zsh<CR>

" Allow for the esc key to work in the opened terminal so we can navigate back to vim.
tnoremap <C-[> <C-\><C-n>

" Change 2 split windows from vertical to horizontal on splits.
map <leader>th <C-w>t<C-w>H
map <leader>tk <C-w>t<C-w>K

"Window/split resizing
noremap <silent> <Esc><Up> :resize +3<CR>
noremap <silent> <Esc><Down> :resize -3<CR>
noremap <silent> <Esc><Left> :vertical resize +3<CR>
noremap <silent> <Esc><Right> :vertical resize -3<CR>

"Exit out of insert mode... faster that Ctrl + ]
inoremap jj <ESC>

"NerdTREE stuff
"
let NERDTreeShowHidden=1
" Start NERDTree and leave the cursor in it.
"autocmd VimEnter * NERDTree

" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
"autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 | let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif

" Close the tab if NERDTree is the only window remaining in it.
"autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

"Navigation betweens splits (windows)
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l


