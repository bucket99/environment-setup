## Notes and dotfiles for setting up a neovim + zsh development environment. ##

### Basic Requirements ###
* Install [Iterm2](https://iterm2.com/) (supports colors and icons that regular termal doesn't)
* Install [Neovim](https://neovim.io/)
* Install ZSH, (Should already be installed on macOS, but we just need to make sure that it's enable by default), If you want to style
the terminal, make sure to install fonts, and then install [powerlevel 10k](https://github.com/romkatv/powerlevel10k))

### ZSH Plugins ###

Some great plugins to get started: https://travis.media/top-10-oh-my-zsh-plugins-for-productive-developers/

**Note:** The only plugin that needed to be installed: (https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md#oh-my-zsh)

**Note:** To make sure **zsh profile settings are loaded** when opening a shell from within vim... create a softlink
```console
sudo ln -s .zshrc /etc/.zshenv
```
Doing this will allow any zsh terminal that's opened within neovim to correctly read and load all of the settings that are configured within ~/.zshrc (themese, plugins, aliases etc)
See: https://stackoverflow.com/questions/11415428/terminal-vim-not-loading-zshrc) 


### Neovim config  ###

Configuration of neovim (set configs/vim script/plugin configuration) will all be done in the following file. 
```console
~/.config/nvim/init.vim
```

**Note:** Neovim setup should create the required configuration directories, but directories/files that 
are missing can be created by hand, including **init.vim** 

We can also optionally create the directory for including lua config in the same place that we configure nvim... for example 

```console
mkdir  ~/.config/nvim/lua 
```

With this directory in place we can call lua scripts (rather than using embedded lua scripts) with the require() function

See also: https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/ 


### Neovim plugins  ###

I've found that I need to wq nvim after adding plugin declarations, and then reopen it and execute **:PlugInstall** to actually get the plugin installed. I'm sure 
there's a way to fix this but I'm lazy. 

Once **init.vim** is open, you can install the plugins by running **:PlugInstall**. Make sure to Run **:checkhealth** after installing a plugin 
to make sure that it's configured correctly. 

#### Telescope  *(fuzzy finder/file grep)*: ####
This plugin can do lots, but mostly we want it in order to perform fuzzy find operations for files project wide, in addition
to fuzzy grep operations on files project wide. In order for these features to work, the  [telescope](https://github.com/nvim-telescope/telescope.nvim) 
plugin requires that the following utilities be installed on the system.)

* [ripgrep](https://github.com/BurntSushi/ripgrep) (brew install ripgrep)
* [fd](https://github.com/sharkdp/fd) (brew install fd)

#### fugitive *(git integration*): ####
Given that it's easy to open a fully fledged zsh terminal window (including all aliases, themes, and plugins) in a split within nvim, the usefulness of this plugin 
seems kind of questionable (however that might be because I was already used to using the commandline for basically all git related operations). However, the 
blame feature is quite useful and provides a nice approximiation of gutter based annotated history/blame found within other IDEs.

### CoC ###
This is a huge plugin that can do lots of stuff, but we're using it to implement autocomplete via LSP extensions.

Start with the installation [instructions](https://github.com/neoclide/coc.nvim) and
list of language [extensions](https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions)

Also note that the readme suggests a config to be used, however it might be worth using it out of the box first to get an idea of what might be misssing since
the configuration is a bit complicated. There a nice [video](https://www.youtube.com/watch?v=OXEVhnY621M) that goes over configuration and setting up language extensioins as well. 

There's also this [article](https://ramezanpour.net/post/2021/04/24/My-ultimate-Neovim-configuration-for-Python-development) that goes over coc configs and other nvim related stuff.

**Note:** An example of something that's missing without additional config is the selection of autocomplete suggestions. This is covered by the config outlined in the coc README

 
### TODO: ###

Features/capabilities offered by traditional IDEs that still need to be installed/configured. 

* pairs
* code completion 
* bash lsp
* yaml lsp
* python lsp
* spelling slp
* debugging
* block comments
* block move

